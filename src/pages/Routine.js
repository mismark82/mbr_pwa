import { withRouter } from 'react-router-dom'
import React, {useState,useEffect} from 'react'
import { Card, Container, Accordion as Chord, Button, Carousel} from 'react-bootstrap'
import Image from 'react-bootstrap/Image'
import Product from "./components/parts/Product"

function Routine(props) {

    const routineID = props.match.params.id


    const [steps, setSteps] = useState([])
    const [routine, setRoutine] = useState([])

        useEffect(()=>{
            function getFetchUrl()
            {
                return "http://workspace2.milolab.it:8000/api/step/?beautyroutine="+ routineID + "&format=json"
            }

            fetch(getFetchUrl()).then((response)=>{
                response.json().then((result)=>
                {
                    setSteps(result.results)
                })
            }).catch(error=>{
                console.log("offline mode")
                
            })
        }, [routineID])

        useEffect(()=>{
            function getFetchUrl()
            {
                return "http://workspace2.milolab.it:8000/api/beautyroutine/"+ routineID + "?format=json"
            }

            fetch(getFetchUrl()).then((response)=>{
                response.json().then((result)=>
                {
                    setRoutine(result)
                })
            }).catch(error=>{
                console.log("offline mode")
                
            })
        }, [routineID])

    return (
        <div className="main-section">
            <h1 className="page-title">{routineID}/{routine.name}</h1>
            <Container>
                <Card className="routine-detail">
                    <Card.Body className="routine-detail-body">
                        <div dangerouslySetInnerHTML={ {__html: routine.description} } />
                        <Chord>
                        {steps.map((step,i)=>
                            parseInt(step.beautyroutine) === parseInt(routineID) && (
                            <Chord.Item key={step.id} eventKey={step.number}>
                                <Chord.Header>
                                <Image className="routine-detail-icon"  src={"/images/step" + step.number + ".png"} thumbnail/> 
                                <h4 className="accordion-title">{step.name}</h4>
                                </Chord.Header>
                                <Chord.Body>
                                    <Carousel fade>
                                    {step.products.map((product,j)=>
                                            parseInt(product) > 0 && (
                                            <Product id={product} key ={product}/>
                                            )
                                        )
                                        }
                                    <div dangerouslySetInnerHTML={ {__html: step.description} } />
                                    </Carousel>
                                </Chord.Body>
                            </Chord.Item>
                            )
                            )}
                        </Chord>
                    </Card.Body>
                    <Card.Footer className="routine-detail-footer">
                        <Button className="routine-card-button" href={"/routines"} variant="outline-primary">Torna alla lista</Button>{' '}
                    </Card.Footer>
                </Card>
            </Container>

        </div>
    )
}
export default withRouter(Routine)