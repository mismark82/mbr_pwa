
import React, {useState,useEffect} from 'react'
import {Card, Col, Row, Container, Button} from 'react-bootstrap'



export default function RoutineList()
{

    const [routines, setRoutines] = useState([])

    useEffect(()=>{
        let url = "http://workspace2.milolab.it:8000/api/beautyroutine/?format=json"
        fetch(url).then((response)=>{
            response.json().then((result)=>
            {
                setRoutines(result.results)
            })
        }).catch(error=>{
            console.log("offline mode")
            
        })
    }, [])

    return(
        <div className="main-section">
         <h1 className="page-title">Le vostre Routines</h1>
            <Container>
                <Row xs={1} md={1} className="routines">
                    {
                        routines.map(
                            (item)=>
                            <Col key={item.id} >
                                <Card className="routine-card">
                                    <Card.Header className="routine-card-header">
                                        <Card.Title className="routine-card-title">
                                            {item.name}
                                            <br/>
                                            {item.gender}
                                            <br/>
                                            {item.type}
                                            <br/>
                                            {new Intl.NumberFormat('it-IT', { style: 'currency', currency: 'EUR' }).format(parseFloat(item.price_from))}&nbsp;-&nbsp;
                                            {new Intl.NumberFormat('it-IT', { style: 'currency', currency: 'EUR' }).format(parseFloat(item.price_to))}
                                            <br/>
                                        </Card.Title>
                                    </Card.Header>
                                    <Card.Body className="routine-card-body">
                                        <p className="routine-card-body-description" dangerouslySetInnerHTML={ {__html: item.description.substring(0,200) + "..."} } />

                                    </Card.Body>
                                    <Card.Footer className="routine-card-footer">
                                        <Button className="routine-card-button" href={"/routine/" + item.id} variant="outline-primary">Leggi Tutto</Button>{' '}
                                    </Card.Footer>
                                </Card>
                            </Col>
                        )
                    }
                </Row>
            </Container>
        </div>
    )

}
