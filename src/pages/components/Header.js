import React from 'react'
import {Navbar, Nav, Image} from 'react-bootstrap'
import {LinkContainer} from 'react-router-bootstrap'
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";


//import Clock from './parts/Clock'

import Routine from '../Routine'
import RoutineList from '../RoutineList'
import About from '../About'
import Home from '../Home'
//import PageNotFound from '../PageNotFound'


class Header extends React.Component
{
    

    render()
    {
        return (
        <Router>
            <Navbar className="container-fluid" collapseOnSelect expand="lg" bg="light" variant="light">
                <LinkContainer to="/">
                    <Navbar.Brand><Image src="/images/logo.png" alt="My Beauty Routine" height="55px"/></Navbar.Brand>
                </LinkContainer>
                <Navbar.Toggle className="ml-auto" aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        <LinkContainer to="/routines">
                            <Nav.Link>Le Routine</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to="/about">
                            <Nav.Link>Cerca</Nav.Link>
                        </LinkContainer>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/about" component={About} />
                <Route path="/routine/:id" component={Routine} />
                <Route path="/routines" component={RoutineList} />
            </Switch>
            
        </Router>
        );
      }
}
export default Header
