import { Carousel, Image, Button } from 'react-bootstrap'
import React from 'react'


class Product extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            error: null,
            isLoaded: false,
            product: null,
            id: this.props.id
        };
    }

    componentDidMount() {
        fetch("http://workspace2.milolab.it:8000/api/product/" + this.state.id + "?format=json")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        product: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {

        const { error, isLoaded, product } = this.state;
        if (error) 
        {
            return (<Carousel.Item>
                <Carousel.Caption>
                    <h3>Errore: {error.message}</h3>
                </Carousel.Caption>
            </Carousel.Item>)
        } 
        else if (!isLoaded) 
        {
            return (<Carousel.Item>
                <Carousel.Caption>
                    <h3>Solo un attimo...</h3>
                </Carousel.Caption>
            </Carousel.Item>)
        } 
        else 
        {
            return (
                <div key={product.id}>
                    <div>
                    <Image src={product.image_url} alt={product.name} thumbnail />
                        <h3>{product.name}</h3>
                        <p dangerouslySetInnerHTML={{ __html: product.price }} />
                    </div>
                    <Button style ={{"background-color" : "rgb(180, 179, 223)" , "border-color" : "rgb(180, 179, 223)"}}> <b> Aggiungi al carrello </b> </Button>
                </div>
            )
        }
    }
}
export default Product;
