import React, {useState,useEffect} from 'react'
import { Accordion as Chord, Image, Carousel, Button} from 'react-bootstrap'

export default function Step()
{
    return(
            <Chord.Item eventKey="1">
                <Chord.Header>
                <Image className="routine-detail-icon"  src="/images/step1.png" thumbnail/> 
                <h4 className="accordion-title">Step Name</h4>
                </Chord.Header>
                <Chord.Body>
                    <Carousel>
                        <Carousel.Item>
                            <img
                            className="d-block w-100"
                            src="public\images\step1.png"
                            alt="First slide"
                            />
                            <Carousel.Caption>
                            <h3>First slide label</h3>
                            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                            </Carousel.Caption>
                            <Button>DAIOHKEN</Button>
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                            className="d-block w-100"
                            src="public\images\step1.png"
                            alt="Second slide"
                            />

                            <Carousel.Caption>
                            <h3>Second slide label</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                            className="d-block w-100"
                            src="public\images\step1.png"
                            alt="Third slide"
                            />

                            <Carousel.Caption>
                            <h3>Third slide label</h3>
                            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                            </Carousel.Caption>
                        </Carousel.Item>
                    </Carousel>
                </Chord.Body>
            </Chord.Item>
    )

}